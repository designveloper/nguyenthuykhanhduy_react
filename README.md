# REACT

## What is react

### Efficient rendering with React

* DOM Diffing

    + Compares rendered content with the new UI changes

    + Make only the minimal changes necessary

    + Compares Javascript objects

    + Is fater than writing to or reading from DOM

## Intro to JSX and Babel

### Pure React

```
const title = React.createElement({
    <tag>,
    { <attributes> },
    <text>
});

render(
    title,
    document.getElementById(<id>);
)
```

### Creating components with ES6 class syntax

```
class SkiingDay extends Component {

}
```

### Creating stateless functional components
```
const SkiingDay = (props) => {

}
```

### Stateless component vs Pure component vs Stateful component

* Stateless component: these are the component which doesn't have life-cycle

* Normal Components: have life-cycle and states

* Pure component: these are the items which have life-cycle and they will always return the same result when a specific set of props is given


## Props and State
* props are components's properties or inputs and immutable.
* state is mutable

### Initial State

* Initial value for states

```
class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {count: 0};
  }
}

var Counter = createReactClass({
  getInitialState: function() {
    return {count: 0};
  },
  // ...
});
```

### Default Props

* defaultProps: default values for props
```
ComponentName.defaultProps = {
  name: 'Stranger'
};
```


### Validating with React.PropTypes
```
SkiDayCount.propTypes = {
    total: PropTypes.number,
    powder: PropTypes.number,
    backcountry: PropTypes.number
}
```

### Custom validation
```
SkiDayList.propTypes = {
    days: function(props) {
        if (!Array.isArray(props.days)) {
            return new Error();
        } else if (!props.days.length) {
            return new Error();
        }
    }
}
```

### Using State Correctly

* Do Not Modify State Directly
* State Updates May Be Asynchronous
* State Updates are Merged

### The Data Flows Down
* Any state is always owned by some specific component, and any data or UI derived from that state can only affect components “below” them in the tree.

## The component life cycle

    + constructor

    + componentWillMount

    + render

    + componentDidMount

    + shouldComponentUpdate

    + componentWillReceiveProps

    + componentWillUpdate

    + componentDidUpdate

    + componentWillUnmount

* New:
    + Deprecated functions: 

        + componentWillReceiveProps

        + componentWillUpdate

    + New functions:

        + getDerivedStateFromProps: is invoked after a component is instantiated as well as before it is re-rendered. Replacement for componentWillReceiveProps

        + getSnapshotBeforeUpdate: is called right before mutations are made (e.g. before the DOM is updated)

### Code-Splitting

* Code-Splitting is a feature supported by bundlers like Webpack and Browserify (via factor-bundle) which can create multiple bundles that can be dynamically loaded at runtime.

* Error Boundaries: catch JavaScript errors anywhere in their child component tree, log those errors, and display a fallback UI

* Higher-Order Components: a function that takes a component and returns a new component.

* Optimizing Performance: 

    + Use the Production Build: UglifyJsPlugin.

        + Concatenation is just appending all of the static files into one large file.

        + Minification is just removing unnecesary whitespace and redundant / optional tokens like curlys and semicolons, and can be reversed by using a linter.

        + Uglification is the act of transforming the code into an "unreadable" form, that is, renaming variables/functions to hide the original intent... It is, also, irreversable.

    + Profiling Components with the Chrome Performance
    
    + Virtualize Long Lists

    + Avoid Reconciliation

* Portals
